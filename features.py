import sys
import math
import scipy.ndimage as nimg
from matplotlib import pyplot as plt
import numpy as np
import scipy.stats as st
from utils.images import *
from utils.vectors import *

toPlot = np.vectorize(lambda x: x*15 if x > 1 else 0)
positive = np.vectorize(lambda x: x if x > 0 else 0)

'''
Corners detection part
'''

def response_function(image_raw, si, sd, debug=False):
    image = np.array(image_raw).astype(np.dtype("f4"))
    Ix = nimg.filters.gaussian_filter(image, sd, [1, 0]).astype(np.dtype("f4"))
    Iy = nimg.filters.gaussian_filter(image, sd, [0, 1]).astype(np.dtype("f4"))
    Ix2 = nimg.filters.gaussian_filter(Ix * Ix, si, 0).astype(np.dtype("f4"))
    Iy2 = nimg.filters.gaussian_filter(Iy * Iy, si, 0).astype(np.dtype("f4"))
    Ixy = nimg.filters.gaussian_filter(Ix * Iy, si, 0).astype(np.dtype("f4"))
    R = Ix2*Iy2 - np.power(Ixy, 2) - 0.05*np.power(Ix2+Iy2, 2)
    return R


def get_corners(R, nsize=9, threshold=1):
    is_max = np.zeros(R.shape)
    meets_threshold = np.zeros(R.shape)
    filtered_max = nimg.filters.maximum_filter(R, nsize)
    is_max = (R == filtered_max)
    meets_threshold = (filtered_max > threshold)
    is_max = is_max * (meets_threshold)
    labeled, num_objects = nimg.label(is_max)
    xy = np.array(nimg.center_of_mass(R, labeled, range(1, num_objects+1)))
    return sorted(xy, key=lambda x: R[x[0], x[1]], reverse=True)


def local_supression(candidates, R):
    suppressed = []
    for i in range(len(candidates)):
        sRadius = 100000
        for j in range(0, i):
            if sRadius > dist(candidates[i], candidates[j]) and \
               R[candidates[i][0], candidates[i][1]]*1.1 <= \
               R[candidates[j][0], candidates[j][1]]:
                    sRadius = dist(candidates[i], candidates[j])
        suppressed.append((sRadius, candidates[i]))
    return sorted(suppressed, key=lambda x: x[0], reverse=True)


def best_corners(testImg, number=-1):
    R = response_function(testImg, 2, 2)
    candidates = get_corners(R)
    evaluated = local_supression(candidates, R)
    print("Found {} corners".format(len(evaluated)))
    return list(map(lambda x: x[1], evaluated[0:number]))


def scale_inv(image, listOfCorners, returnBlurred=False):
    # SCALE PARAMETERS
    sigma_begin = 1.5
    sigma_step = 1.2
    sigma_nb = 13
    # sigmas for all different scales
    sigma_I = np.array([sigma_begin*sigma_step**i for i in range(sigma_nb)])
    # sigmas for derivatives
    sigma_D = 0.7 * sigma_I
    responses = np.zeros((len(listOfCorners), sigma_nb))
    blurred = []
    for s, (si, sd) in enumerate(zip(sigma_I, sigma_D)):
        resp = response_function(image, si, sd)*si**2
        if returnBlurred:
            blurred.append(nimg.gaussian_filter(image, si))
        for i, corner in enumerate(listOfCorners):
            responses[i, s] = resp[corner[0], corner[1]]
    withScales = list(map(lambda x: (sigma_I[np.argmax(responses[x, :])],
                                     listOfCorners[x]),
                          range(len(listOfCorners))))
    if returnBlurred:
        withScales = list(map(lambda x: (np.argmax(responses[x, :]),
                                         listOfCorners[x]),
                          range(len(listOfCorners))))

        return withScales, sigma_I, blurred
    else:
        return withScales

'''
Feature description part
'''

def get_feature_orientations(image, features):
    grad = np.gradient(image.astype('float'))
    bins = 36
    da = 2*np.pi/bins
    # features = map(lambda  x: np.array([x[0], x[1], 0]),  features2d)
    orientations = np.zeros((len(features), 36))
    dxy = np.zeros(3)
    for fnum, f in enumerate(features):
        print(fnum, end = ' ')
        for i in range(-20, 21):
            for j in range(-20, 21):
                if f[0]+i < 0 or f[0]+i >= image.shape[0] or f[1]+j < 0 or f[1]+j >= image.shape[1]:
                    continue
                dxy[0] = grad[0][int(f[0]+i), int(f[1]+j)]
                dxy[1] = grad[1][int(f[0]+i), int(f[1]+j)]
                a = math.floor((math.atan2(dxy[0], dxy[1]) + math.pi) * bins / 2 / math.pi) - 1
                sv = np.array([np.sin(da*a), np.cos(da*a), 0])
                ev = np.array([np.sin(da*(a+1)), np.cos(da*(a+1)), 0])

                orientations[fnum, a] += np.abs(np.dot(dxy, sv))
                orientations[fnum, (a+1) % bins] += np.abs(np.dot(dxy,ev))
        #print_orientation(orientations[fnum,:], 36)
        #show(image[f[0]-20:f[0]+20, f[1]-20:f[1]+20])

    return np.argmax(orientations, axis=1)*da


def orientation_histogram(grad, bins):
    orientations = np.zeros(bins)
    dxy = np.zeros(3)
    for i in range(grad[0].shape[0]):
        for j in range(grad[0].shape[1]):
            dxy[0] = grad[0][i, j]
            dxy[1] = grad[1][i, j]
            da = 2*np.pi/bins
            for a in range(0, bins):
                sv = np.array([np.sin(da*a), np.cos(da*a), 0])
                ev = np.array([np.sin(da*(a+1)), np.cos(da*(a+1)), 0])
                # This means, that vector is between sv and ev
                if np.cross(dxy, sv)[2] > 0 and np.cross(dxy, ev)[2] < 0:
                    orientations[a] += np.abs(np.dot(dxy, sv))
                    orientations[(a+1) % bins] += np.abs(np.dot(dxy, ev))
                    break
    return orientations


def print_orientation(orientations, bins):
    plt.clf()
    da = 2*np.pi/bins
    for a in range(0, bins):
        sv = np.array([np.sin(da*a), np.cos(da*a)])*orientations[a]
        plt.plot([0, sv[0]], [0, sv[1]])
    plt.show()


import scipy.stats as st
def gkern(kernlen=21, nsig=3):
    """Returns a 2D Gaussian kernel array."""
    interval = (2*nsig+1.)/(kernlen)
    x = np.linspace(-nsig-interval/2., nsig+interval/2., kernlen+1)
    kern1d = np.diff(st.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    kernel = kernel_raw/kernel_raw.sum()
    return kernel


def get_sift(values):
    bins = 8
    ldiv = 4
    x, y = values.shape
    grad = np.gradient(values)
    mask = gkern(x, 0.67)
    mask /= np.amax(mask)
    grad[0] *= mask
    grad[1] *= mask
    orientations = np.zeros((ldiv, ldiv, bins))
    dxy = np.zeros(3)
    for ib in range(ldiv):
        for jb in range(ldiv):
            pgrad = [grad[i][(ib*x//ldiv):((ib+1)*x//ldiv-1), (jb*y//ldiv):((jb+1)*y//ldiv-1)] for i in [0,1]]
            orientations[ib, jb, :] = orientation_histogram(pgrad, bins)

    raw_sift = np.reshape(nimg.filters.gaussian_filter(orientations, [2, 2, 0], 0), -1)
    if sum(raw_sift)==0:
        return raw_sift
    norm_sift = raw_sift/sum(raw_sift)
    norm_sift = list(map(lambda x: 0.2 if x>0.2 else x, norm_sift))
    norm_sift = norm_sift/sum(norm_sift)
    return norm_sift

def get_descriptors(image, features, blurred=None, scales=None):
    sift = np.zeros((len(features), 128))
    angles = get_feature_orientations(image, np.array(list(map(lambda x: x[1], features))))
    print("Orientations computed!")
    for i, (scale, f) in enumerate(features):
        angle = angles[i]
        print("Feature no {}. Angle {}, scale {}".format(i, angle, scales[scale]/1.5))
        sampling = getSamplingPoints(angle, scales[scale]/1.5)
        if blurred is None:
            sampledImage = image
        else:
            sampledImage = blurred[scale]
            scale = scales[scale]
        values = nimg.interpolation.map_coordinates(sampledImage, [f[0]+sampling[:,0], f[1]+sampling[:,1]])
        patch = np.reshape(values, (16, 16))
        #show(patch)
        sift[i] = get_sift(patch)
    return sift

def print_sift_squares(image, features, blurred=None, scales=None):
    angles = get_feature_orientations(blurred[0], list(map(lambda x: x[1], features)))
    with_squares = image.copy()
    print("Orientations computed!")
    for i, (scale, f) in enumerate(features):
        angle = angles[i]
        print("Feature no {}. Angle {}, scale {}".format(i, angle,  scales[scale]/1.5))
        sampling = getSamplingPoints(angle,  scales[scale]/1.5)
        cv2.circle(with_squares, (int(f[1]), int(f[0])), 3,(255,0,0), 2)
        for x, y in sampling:
            cv2.circle(with_squares, (int(f[1]+x), int(f[0]+y)), 1, 1)
    show(with_squares)

def nndm(d1, d2, n):
    d1_2 = np.tile(np.sum(d1*d1, axis=1).transpose(), (len(d2),1)).transpose()
    d2_2 = np.tile(np.sum(d2*d2, axis=1).transpose(), (len(d1),1))
    d12 = np.dot(d2, d1.transpose()).transpose()
    dst = np.sqrt(d1_2 + d2_2 - 2*d12).T
    print(dst)
    assignment = np.argmin(dst, 1)
    print(assignment)
    kord = np.partition(dst, 1)
    ratio = kord[:, 1] / kord[:,0]
    print(kord)
    print("Ratio", ratio)
    best = np.argsort(ratio)[-n:]
    print("Best", best, ratio[best])
    mask = -np.ones(assignment.shape).astype('int32')
    mask[best] = 1
    return assignment * mask


if __name__ == "__main__":
    if len(sys.argv)>2:
        img_filename = sys.argv[1]
        corners = best_corners(img_filename)
        #imgTwo = sys.argv[2]


