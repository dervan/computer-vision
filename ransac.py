import utils.vectors as vec
import numpy as np
import cv2
import numpy.random as rnd
import matrices.essential as es
import numpy.linalg as lin
import utils.vectors as vec

K = np.array( [[2759.48,0.00000,1520.69], [0.00000,2764.16,1006.81], [0.00000,0.00000,1.00000]])
def essential(p0, p1, it):
    n = len(p1)
    ns = np.arange(n)
    pk0 = np.dot(lin.inv(K), p0.T).T
    pk1 = np.dot(lin.inv(K), p1.T).T
    bestScore = 1000000
    bestE = None
    for i in range(it):
        samples = rnd.choice(ns, 8)
        E = es.getE(8, pk0[samples], pk1[samples])
        print("No.", samples, "E:", E, sep = '\n')
        out = 0
        for pp1, pp2 in zip(p0, p1):
            if vec.ldist(np.dot(E, pp1), pp2) > 0.1:
                out+=1
        if out < bestScore:
            bestScore = out
            bestE = E
        print("Outliers: ", out)
        print()
    print("Best outliners: ", bestScore)
    print("Best E:\n", bestE)
    return bestE

