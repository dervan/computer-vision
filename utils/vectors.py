import numpy.linalg as lin
import numpy as np
import scipy.stats as st

def ldist(l, p):
    return abs(l[0]*p[0]+l[1]*p[1]+l[2])/(l[0]**2+l[1]**2)

def hom(v):
    return v/v[-1]

def dist(p1, p2):
    return np.sqrt((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)

def rotate(vec, a):
    x,y = vec
    return np.array([x*np.cos(a)-y*np.sin(a), x*np.sin(a)+y*np.cos(a)])

def getSamplingPoints(a, s):
    samplingX = np.arange(-8,8)
    samplingY = np.arange(-8,8)
    xx,yy = np.meshgrid(samplingX, samplingY)
    listXY = list(zip(xx.reshape(-1), yy.reshape(-1)))
    return np.array(list(map(lambda x: rotate(x, a) * s, listXY)))

def getCoords(start, end, step):
    samplingX = np.arange(start[1], end[1], step)
    samplingY = np.arange(start[0], end[0], step)
    xx, yy = np.meshgrid(samplingX, samplingY)
    return np.dstack([xx,yy])
