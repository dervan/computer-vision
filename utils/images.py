import cv2
import scipy.ndimage as nimg
from matplotlib import pyplot as plt
import numpy as np
import scipy.stats as st

defaultFigSize = (22, 22)


def load_gray(imgName):
    image = cv2.imread(imgName)
    return cv2.cvtColor(image, cv2.COLOR_RGB2GRAY).astype('float')


def load_color(imgName):
    image = cv2.imread(imgName)
    return cv2.cvtColor(image, cv2.COLOR_BGR2RGB)


def normalize(img):
    nmin = np.min(img)
    nmax = np.max(img)
    return 255*(img-nmin)/(nmax-nmin)


def show(img, times=1):
    plt.figure(figsize=(defaultFigSize[0]*times, defaultFigSize[1]*times))
    plt.imshow(img)
    plt.show()


def blur(img, sig):
    return nimg.gaussian_filter(img, sig, 0)


def show_points(image, points, r=2, w=1, color=(255, 10, 10)):
    withPoints = image.copy()
    for u in points:
        cv2.circle(withPoints, (int(u[1]), int(u[0])), r, color, 2)
    show(withPoints)



def show_points_size(image, points, colors=None):
    with_points = image.copy()
    for i, (point, r) in enumerate(points):
        if colors is None:
            c = (255, 80, 50)
        else:
            c0 = (255, 0, 0)
            c = tuple(map(int, colors[i % len(colors)]))
        if not isinstance(point, tuple):
            tmp = point
            point = r
            r = tmp
        cv2.circle(with_points, (int(point[1]), int(point[0])), int(r), color=c, thickness=3)
        cv2.circle(with_points,
                   (int(point[1]), int(point[0])), 1, (150, 80, 50), 2)
    show(with_points)

def show_correspondences(left, right, lpoints, rpoints, colors=None):
    print(left.shape, right.shape)
    width = int(np.ceil(max(left.shape)*0.001))
    if(left.shape[0] <= right.shape[0]):
        newLeft = np.zeros((right.shape[0], left.shape[1], 3))
        newLeft[0:left.shape[0]] = left[:]
        left = newLeft
    elif(left.shape[0] > right.shape[0]):
        newRight= np.zeros((left.shape[0], right.shape[1], 3))
        newRight[0:right.shape[0]] = right[:]
        right = newRight

    img = np.hstack([left, right])
    shift = left.shape[1]
    for i, ((lpoint, lr), (rpoint, rr))  in enumerate(zip(lpoints, rpoints)):
        if colors is None:
            c = (255, 80, 50)
        else:
            c0 = (255, 0, 0)
            c = tuple(map(int, colors[i % len(colors)]))
        if not isinstance(lpoint, tuple):
            tmp = lpoint
            lpoint = lr
            lr = tmp
        if not isinstance(rpoint, tuple):
            tmp = rpoint
            rpoint = rr
            rr = tmp
        cv2.circle(img, (int(lpoint[1]), int(lpoint[0])), int(lr), color=c, thickness=3)
        cv2.circle(img, (int(rpoint[1]+shift), int(rpoint[0])), int(rr), color=c, thickness=3)
        cv2.circle(img, (int(lpoint[1]), int(lpoint[0])), 1, (150, 80, 50), 2)
        cv2.circle(img, (int(rpoint[1]+shift), int(rpoint[0])), 1, (150, 80, 50), 2)
        cv2.line(img, (int(lpoint[1]), int(lpoint[0])),(int(rpoint[1]+shift), int(rpoint[0])), color=c, thickness=width)
    show(img.astype('uint8'), 2)



def gaussian_kernel(kernlen=16, nsig=1):
    interval = (2*nsig+1.)/(kernlen)
    x = np.linspace(-nsig-interval/2., nsig+interval/2., kernlen+1)
    kern1d = np.diff(st.norm.cdf(x))
    kernel_raw = np.sqrt(np.outer(kern1d, kern1d))
    kernel = kernel_raw/kernel_raw.sum()
    return kernel

def color(gray_img):
    return cv2.cvtColor(gray_img, cv2.COLOR_GRAY2RGB)

def ssd(imgA, imgB, a, b, patchsize):
    ym = np.floor(min(patchsize, a[0], b[0])).astype('int32')
    xm = np.floor(min(patchsize, a[1], b[1]))
    yp = np.floor(min(patchsize, imgA.shape[0]-a[0]-1, imgB.shape[0]-b[0]-1))
    xp = np.floor(min(patchsize, imgA.shape[1]-a[1]-1, imgB.shape[1]-b[1]-1))
    return np.sum(np.abs(imgA[a[0]-ym:a[0]+yp+1, a[1]-xm:a[1]+xp+1] -
                         imgB[b[0]-ym:b[0]+yp+1, b[1]-xm:b[1]+xp+1])) / ((ym+yp+1)*(xm+xp+1))
def pixelDist(X,Y):
    return np.exp(-np.linalg.norm(X-Y, ord=1)/100)*np.linalg.norm(X-Y)



def matchCost(imgA, imgB, a, b, patchsize):
    ym = np.floor(min(patchsize, a[0], b[0])).astype('int32')
    xm = np.floor(min(patchsize, a[1], b[1])).astype('int32')
    yp = np.floor(min(patchsize, imgA.shape[0]-a[0]-1, imgB.shape[0]-b[0]-1)).astype('int32')
    xp = np.floor(min(patchsize, imgA.shape[1]-a[1]-1, imgB.shape[1]-b[1]-1)).astype('int32')
    return np.sum(np.abs(imgA[a[0]-ym:a[0]+yp+1, a[1]-xm:a[1]+xp+1] -
                         imgB[b[0]-ym:b[0]+yp+1, b[1]-xm:b[1]+xp+1]) * np.exp(-(imgB[b[0]-ym:b[0]+yp+1, b[1]-xm:b[1]+xp+1] - imgA[a[0], a[1]])/100))  / ((ym+yp+1)*(xm+xp+1))

def compute_stereo_energy(disp, left, right, patchsize, penalty, maxd):
    y, x = disp.shape
    energy = 0.0
    ngh = [(-1,0), (1, 0), (0, -1), (0, 1)]
    for i in range(1,y-1):
        for j in range(1, x-maxd):
            if(j + disp[i,j] < x):
                energy += (float(left[i,j])-float(right[i,j+disp[i,j]]))**2
            energy += np.sum([penalty * (disp[i+di, j+dj] != disp[i, j]) for di, dj in ngh])
    return energy

