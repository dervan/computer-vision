import utils.vectors as vec
import utils.images as img
import numpy as np
import numpy.random as nrand
import scipy.ndimage as ndimg
import cv2
import random
import sys

with open(sys.argv[1]) as f:
    lines = f.readlines()
    leftFile = lines[0].rstrip("\n")
    rightFile = lines[1].rstrip("\n")
    patchsize = int(lines[2])
    disparities = int(lines[3])

left = img.load_gray(leftFile)
right = img.load_gray(rightFile)
cleft = left
cright = right
#cleft = img.load_color(leftFile)
#cright = img.load_color(rightFile)
#print(cleft.shape)
#left = img.load_gray("./tskuba/scene1.row3.col2.ppm")
#right = img.load_gray("./tskuba/scene1.row3.col1.ppm")
# left = img.load_gray("./stereo/data/rectified/Sport_R_1.png")
# right = img.load_gray("./stereo/data/rectified/Sport_R_0.png")
corresponencePropagation = False
maxshift = disparities
shiftstep = 0.5
bonus = 0.8
penalty = (2*patchsize+1)*255**2


# coords = vec.getCoords((0, 0), left.shape, 1)

ldepth = nrand.randint(0, disparities, left.shape, dtype=np.dtype('int32'))
rdepth = nrand.randint(-disparities, 0, left.shape, dtype=np.dtype('int32'))
# depth = np.ones(left.shape)
empty = np.zeros(left.shape)
#shift = np.dstack([depth, empty])
# sampling = coords + shift
# r = sampling.reshape(-1, 2)
# print(r.shape, r[:, 1].shape)

# depthimg = ndimg.map_coordinates(left, [r[:, 1],r[:, 0]]).reshape(left.shape)
# img.show(np.hstack([depthimg, depth]))

ydim, xdim = left.shape

scores = np.zeros(left.shape)

for it in range(5):
    if it % 2:
        d = 1
        yrange = range(ydim-1, -1, -1)
        xrange = range(xdim-1, -1, -1)
    else:
        d = -1
        yrange = range(0, ydim)
        xrange = range(0, xdim)
    print("Iteration ", it)
    for y in yrange:
        for x in xrange:
            #  Spacial propagation
            if(x+ldepth[y, x] < xdim):
                val = img.matchCost(cleft, cright, (y, x), (y, x+ldepth[y, x]), patchsize)
            else:
                val = penalty
            if(x+d > 0 and x+d < xdim and x+ldepth[y, x+d] < xdim):
                valx = img.matchCost(cleft, cright, (y, x), (y, x+ldepth[y, x+d]), patchsize)*bonus
            else:
                valx = penalty
            if(y+d > 0 and y + d < ydim and x + ldepth[y + d, x] < xdim):
                valy = img.matchCost(cleft, cright, (y, x), (y, x+ldepth[y+d, x]), patchsize)*bonus
            else:
                valy = penalty
            if corresponencePropagation:
                candidates = list(filter(lambda corr: np.abs(rdepth[y, x + corr]) == corr if x+corr<xdim else False,  range(disparities)))
                vals = list(map(lambda step: img.matchCost(cleft, cright, (y, x), (y, x + step), patchsize) if step > 0 and step < maxshift and (x + step) > 0 and ((x + step) < xdim) else penalty,
                       candidates))
            else:
                candidates = []
                vals = []
            #  Save propagation
            base = [val, valx, valy]
            base.extend(vals)
            arg = np.argmin(base)
            if arg == 0:
                pass
            elif arg == 1:
                ldepth[y, x] = ldepth[y, x+d]
                val = valx*1/bonus
            elif arg == 2:
                ldepth[y, x] = ldepth[y+d, x]
                val = valy*1/bonus
            else:
                ldepth[y, x] = candidates[arg-3]
                val = vals[arg-3]

            # Random search
            steps = [random.uniform(-1, 1)*maxshift*shiftstep**i for i in np.arange(np.log(maxshift)/np.log(1.0/shiftstep)+0.001)]
            dx = ldepth[y, x]
            vals = map(lambda step: img.matchCost(cleft, cright, (y, x), (y, x + dx + step), patchsize) if dx+step > 0 and dx+step < maxshift and (x+dx + step) > 0 and ((x+dx + step) < xdim) else penalty, steps)
            vals = list(vals)
            steps.append(0)
            vals.append(val)
            arg = np.argmin(vals) 
            ldepth[y, x] = dx + steps[arg]
            '''
            # And for RIGHT
            #  Spacial propagation
            if(x+rdepth[y, x] >= 0):
                val = img.matchCost(cright, cleft, (y, x), (y, x+rdepth[y, x]), patchsize)
            else:
                val = penalty
            if(x+d > 0 and x+d < xdim and x+rdepth[y, x+d] >= 0):
                valx = img.matchCost(cright, cleft, (y, x), (y, x+rdepth[y, x+d]), patchsize)*bonus
            else:
                valx = penalty
            if(y+d > 0 and y + d < ydim and x + rdepth[y + d, x] >= 0):
                valy = img.matchCost(cright, cleft, (y, x), (y, x+rdepth[y+d, x]), patchsize)*bonus
            else:
                valy = penalty
            if corresponencePropagation:
                candidates = list(filter(lambda corr: np.abs(ldepth[y, x - corr]) == corr if x-corr > 0 else False,  range(disparities)))
                vals = list(map(lambda step: img.matchCost(cright, cleft, (y, x), (y, x - step), patchsize) if step > 0 and step < maxshift and (x - step) > 0 and ((x - step) < xdim) else penalty,
                       candidates))
            else:
                candidates = []
                vals = []
            #  Save propagation
            base = [val, valx, valy]
            base.extend(vals)
            arg = np.argmin(base)
            if arg == 0:
                pass
            elif arg == 1:
                rdepth[y, x] = rdepth[y, x+d]
                val = valx*1/bonus
            elif arg == 2:
                rdepth[y, x] = rdepth[y+d, x]
                val = valy*1/bonus
            else:
                rdepth[y, x] = -candidates[arg-3]
                val = vals[arg-3]

            # Random search
            steps = [random.uniform(-1, 1)*maxshift*shiftstep**i for i in np.arange(np.log(maxshift)/np.log(1.0/shiftstep)+0.001)]
            dx = rdepth[y, x]
            vals = map(lambda step: img.matchCost(cright, cleft, (y, x), (y, x + dx + step), patchsize) if dx+step < 0 and dx+step > -maxshift and (x+dx + step) > 0 and ((x+dx + step) < xdim) else penalty, steps)
            vals = list(vals)
            steps.append(0)
            vals.append(val)
            arg = np.argmin(vals) 
            rdepth[y, x] = dx + steps[arg]
            #if x == 60:
            #    vals = map(lambda step: img.matchCost(right, left, (y, x), (y, x - step)) if (x - step) > 0 else penalty, range(61))
            #    print(list(vals))
            '''
            img2 = np.hstack([ldepth]).astype('float32')/disparities
            #if(y % 10 == 0):

            cv2.imshow(leftFile, img2)
            if(True):
                k = cv2.waitKey(1)
                if k == 27:
                    exit(1)
    print(img.compute_stereo_energy(ldepth, left, right, patchsize, 100))

cv2.waitKey(0)
cv2.waitKey(0)
