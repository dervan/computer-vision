import numpy as np
import scipy.io as io
import numpy.linalg as lin
from PIL import Image
import cv2
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def append(vect, elem):
    return np.hstack([vect, [elem]])

def trs(vect):
    return vect[:, np.newaxis]

def trapp(vect, elem):
    return trs(append(vect, elem))

def uni(vect):
    return vect/vect[-1]

class StereoProcess(object):
    def __init__(self):
        self.Pl = None
        self.Pnl = None
        self.Pr = None
        self.Pnr = None
        self.cl = None
        self.cr = None
        self.lImg = None
        self.rImg = None
        self.Tl = None
        self.Tr = None
        self.b = None
        self.rectlImg = None;

    def setMatrices(self, Pl, Pr):
        self.Pl = Pl
        self.Pr = Pr

    def computeCenters(self):
        self.cl = - np.dot(lin.inv(Pl[:,0:3]), Pl[:,3])
        self.cr = - np.dot(lin.inv(Pr[:,0:3]), Pr[:,3])
        print("Pl * Cl =", np.dot(Pl, append(self.cl, 1)))
        print("Pr * Cr =", np.dot(Pr, append(self.cr, 1)))

    def printCenters(self):
        print("Cl ", self.cl)
        print("Cr ", self.cr)

    def solve(self):
        au = lin.norm(np.cross(Pl[0,0:3], Pl[2,0:3]))
        av = lin.norm(np.cross(Pl[1,0:3], Pl[2,0:3]))
        fl = Pl[2, 0:3]
        fr = Pr[2, 0:3]
        nn = np.cross(fl, fr)
        # First equation set
        A = np.hstack([trapp(self.cl, 1), trapp(self.cr, 1), trapp(nn, 0)]).T
        U, S, Vh = lin.svd(A)
        V = Vh.T
        r = 1/(lin.norm(V[[0, 1, 2], 3]))
        a3 = r * V[:, 3]
        # Second one
        A = np.hstack([trapp(self.cl, 1), trapp(self.cr, 1), trapp(a3[0:3], 0)]).T
        print(A)
        U, S, Vh = lin.svd(A)
        V = Vh.T
        r = lin.norm(av)/(lin.norm(V[[0, 1, 2], 3]))
        a2 = r * V[:, 3]
        A = np.hstack([trapp(self.cl, 1), trapp(a2[0:3], 0), trapp(a3[0:3], 0)]).T
        U, S, Vh = lin.svd(A)
        V = Vh.T
        r = lin.norm(au)/(lin.norm(V[[0, 1, 2], 3]))
        a1 = r * V[:, 3]
        A = np.hstack([trapp(self.cr, 1), trapp(a2[0:3], 0), trapp(a3[0:3], 0)]).T
        U, S, Vh = lin.svd(A)
        V = Vh.T
        r = lin.norm(au)/(lin.norm(V[[0, 1, 2], 3]))
        b1 = r * V[:, 3]
        H = np.diag([1, 1, 1])
        self.Pnl = np.hstack([trs(a1), trs(a2), trs(a3)]).T
        self.Pnr = np.hstack([trs(b1), trs(a2), trs(a3)]).T
        H = np.eye(3)
        H[0,0] = -1
        H[1, 1] = 1
        self.Pnl = np.dot(H, self.Pnl)
        self.Pnr = np.dot(H, self.Pnr)
        self.Tr = np.dot(self.Pnr[0:3, 0:3], lin.inv(self.Pr[0:3, 0:3]))
        self.Tl = np.dot(self.Pnl[0:3, 0:3], lin.inv(self.Pl[0:3, 0:3]))
        tmp = np.dot(self.Pnl, append(self.cl, 1))
        print(tmp)
        tmp = np.dot(self.Pnr, append(self.cr, 1))
        print(tmp)
        #t1 = (np.dot(self.Pl, np.array([0,0,0,1])))
        #t2 = (np.dot(self.Pnl, np.array([0,0,0,1])))
        #print(t1/t1[2], t2/t2[2])
        #tmp = np.dot(self.Tr, self.cr)
        #print(tmp/tmp[2])

    def loadImages(self, lImg, rImg):
        self.lImg = lImg
        self.rImg = rImg

    def apply(self):
        #px = np.meshgrid(range(self.lImg.size[0]), range(self.lImg.size[1]))
        shiftMtx = np.eye(3)
        shiftMtx[0, 2] = 600
        shiftMtx[1, 2] = 300
        print("Applying: \n", self.Tl)
        corners = [(0,0),(384, 288), (768, 576), (576, 768)]
        for c in corners:
            print("{} -> {}".format(c, uni(np.dot(self.Tl, append(c, 1)))))
            print("{} -> {}".format(c, uni(np.dot(self.Tr, append(c, 1)))))
        self.rectlImg = cv2.warpPerspective(self.lImg, np.dot(shiftMtx, self.Tl), (1920, 1080))
        shiftMtx[0, 2] = 600
        self.rectrImg = cv2.warpPerspective(self.rImg, np.dot(shiftMtx, self.Tr), (1920, 1080))
        rectified = cv2.resize(self.rectlImg+self.rectrImg, (1280, 720))
        cv2.imshow('img', rectified)
        cv2.waitKey(0)

    def baseline(self):
        print("B: \n", self.cl - self.cr)
        print("len(B): \n", lin.norm(self.cl - self.cr))
        self.b = lin.norm(self.cl - self.cr)
        
    def rectify_image(img, T):
        width, height = self.lImg.shape
        rect = np.zeros((height + 50, width + 50, 4))
        Y = np.arange(img.shape[0])
        X = np.arange(img.shape[1])
        [YS, XS] = np.meshgrid(X, Y)
        M = np.ones((img.shape[0], img.shape[1], 3))
        M[:, :, 0] = XS
        M[:, :, 1] = YS
        M = M.reshape((img.shape[0] * img.shape[1], 3))

        transformed = T.dot(M.T)
        transformed /= transformed[2, :]
        transformed = transformed[:2, :].T - mins

        lowlow = np.round(transformed - 0.5)
        highhigh = np.round(transformed + 0.5)
        lowhigh = np.array([np.round(transformed[:, 0] - 0.5),
                            np.round(transformed[:, 1] + 0.5)]).T
        highlow = np.array([np.round(transformed[:, 0] + 0.5),
                            np.round(transformed[:, 1] - 0.5)]).T
        roundings = [lowlow, highhigh, lowhigh, highlow]
        for rd in roundings:
            w = rd - transformed
            w = 1.0 / (0.001 + np.sum(w ** 2, axis=1) ** 0.5)
            rd = rd.astype('int32')
            rect[rd[:, 1], rd[:, 0], 3] += w
            rect[rd[:, 1], rd[:, 0], :3] += (img.reshape((w.shape[0], 3)).T * w).T

        rect[:, :, 3][rect[:, :, 3] == 0.0] = 1.0
        rect[:, :, 0] /= rect[:, :, 3]
        rect[:, :, 1] /= rect[:, :, 3]
        rect[:, :, 2] /= rect[:, :, 3]
        rect = rect[:, :, :3].astype('uint8')
        return np.transpose(rect, [1, 0, 2])

    def show(self):
        cv2.imshow('img', self.rectlImg)
        cv2.waitKey(0)
        cv2.destroyAllWindows()

def cv_stereo(left, right):
    window = 7
    stereo = cv2.StereoSGBM_create(minDisparity=0, numDisparities=192, blockSize=window, P1 = window**2*2, P2 = window**2*16)
    disparity = stereo.compute(left,right)
    cdisp = cv2.applyColorMap(disparity, 3)
    plt.imshow(cdisp)
    plt.show()
    #fig = plt.figure()
    #ax = fig.add_subplot(111, projection='3d')
    #plt.show()

Pl = io.loadmat("data/Sport_cam.mat")['pml']
Pr = io.loadmat("data/Sport_cam.mat")['pmr']
p = StereoProcess()
p.setMatrices(Pl, Pr)
p.computeCenters()
p.printCenters()
p.solve()
p.baseline()

p.loadImages(cv2.imread("./data/Sport0.png"), cv2.imread("./data/Sport1.png"))
p.apply()

cv_stereo(cv2.imread("./data/rectified/s0.png", 0), cv2.imread("./data/rectified/s1.png", 0))
exit(1)
#p.show()
