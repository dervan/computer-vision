import utils.vectors as vec
import utils.images as img
import numpy as np
import numpy.random as nrand
import scipy.ndimage as ndimg
import cv2
import random
import sys

with open(sys.argv[1]) as f:
    lines = f.readlines()
    leftFile = lines[0].rstrip("\n")
    rightFile = lines[1].rstrip("\n")
    patchsize = int(lines[2])
    disparities = int(lines[3])

left = img.load_gray(leftFile).astype('uint8')
right = img.load_gray(rightFile).astype('uint8')

maxshift = disparities
shiftstep = 0.5
bonus = 0.95
penalty = (2*patchsize+1)*255**2
# depthimg = ndimg.map_coordinates(left, [r[:, 1],r[:, 0]]).reshape(left.shape)
# img.show(np.hstack([depthimg, depth]))

ydim, xdim = left.shape
window = 2*patchsize+1
print(left.shape, right.shape)
stereo = cv2.StereoSGBM_create(minDisparity=0, numDisparities=disparities, blockSize=window, P1 = window**2*2, P2 = window**2*16)
disparity = stereo.compute(right, left).astype('float32')
#cdisp = cv2.applyColorMap(disparity, 3)
#plt.imshow(cdisp)
disparity = (disparity - np.min(disparity))/(np.max(disparity)-np.min(disparity))*disparities
disparity = np.roll(disparity, -disparities)
print("Energy: " , img.compute_stereo_energy(disparity, left, right, patchsize, 100, disparities))
cv2.imshow("stereo", disparity/np.max(disparity))


cv2.waitKey(0)
cv2.waitKey(0)
