import scipy.io as io
import numpy as np
import numpy.linalg as lin

def dbg(arr):
    print(type(arr))
    if isinstance(arr, np.ndarray):
        print(arr.shape)
'''
calibration = io.loadmat("data/compEx3data.mat")['K']
pointsImages = io.loadmat("data/compEx1data.mat")['x']

# 3 x 2008 arrays
pointsImage1 = pointsImages[0, 0].transpose()
pointsImage2 = pointsImages[1, 0].transpose()
cNorm1 = np.dot(lin.inv(calibration), pointsImage1.transpose()).transpose()
cNorm2 = np.dot(lin.inv(calibration), pointsImage2.transpose()).transpose()
'''

def getE(rows, points1, points2, verbose=False):
    A = np.zeros((rows, 9))
    for r in range(rows):
        A[r,:] = [np.array(uvp) * np.array(uv) for uvp in points1[r] for uv in points2[r]]
    u, s, v = lin.svd(A)#, np.zeros(20))
    fundamentalRank3 = v[-1,:].reshape((3,3))
    u, s, v = lin.svd(fundamentalRank3)
    s[0] = (s[0]+s[1])/2
    s[1] = s[0]
    s[2] = 0
    E = np.dot(u,np.dot(np.diag(s),v))
    return E

def getF(rows, K):
    ic = lin.inv(K)
    E = getE(rows)
    F = np.dot(ic.transpose(), np.dot(E, ic))
    return F
