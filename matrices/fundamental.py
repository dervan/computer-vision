import scipy.io as io
import numpy as np
import numpy.linalg as lin

def dbg(arr):
    print(type(arr))
    if isinstance(arr, np.ndarray):
        print(arr.shape)

pointsImages = io.loadmat("data/compEx1data.mat")['x']
# 3 x 2008 arrays
pointsImage1 = pointsImages[0, 0].transpose()
pointsImage2 = pointsImages[1, 0].transpose()

def getF(rows, points1=pointsImage1, points2=pointsImage2, verbose=False):
    A = np.zeros((rows, 9))
    for r in range(rows):
        A[r,:] = [uvp * uv for uvp in points1[r] for uv in points2[r]]
    u, s, v = lin.svd(A)#, np.zeros(20))
    fundamentalRank3 = v[-1,:].reshape((3,3))
    u, s, v = lin.svd(fundamentalRank3)
    s[2] = 0
    F = np.dot(u,np.dot(np.diag(s),v))
    if verbose:
        print("Fundamental matrix:\n", F)
        for i in range(rows):
            print(np.dot(np.dot(points1[i],F),points2[i].transpose()), " ", np.dot(np.dot(points2[i],F),points1[i].transpose()))
    return F

def getNormalizedF(rows, verbose=False):
    print(pointsImage1[0:5, :])
    mean1 = np.mean(pointsImage1, 0)
    mean2 = np.mean(pointsImage2, 0)
    shifted1 = (pointsImage1 - mean1 + np.array([0,0,0]))
    shifted2 = (pointsImage2 - mean2 + np.array([0,0,0]))
    len1 = np.mean(np.sqrt(np.sum(shifted1*shifted1, 1)))
    len2 = np.mean(np.sqrt(np.sum(shifted2*shifted2, 1)))
    T1 = np.diag([1/len1, 1/len1, 1])
    T2 = np.diag([1/len2, 1/len2, 1])
    T1[0:2, -1] = -mean1[0:2]/len1
    T2[0:2, -1] = -mean2[0:2]/len2
    print("T1:",T1)
    norm1 = np.dot(T1, pointsImage1.transpose()).transpose()
    norm2 = np.dot(T2, pointsImage2.transpose()).transpose()
    print(norm1[0:5, :])
    print("N1 mean", np.mean(norm1))
    F =  getF(rows, norm1, norm2, False)
    F = np.dot(np.dot(T2, F), T1)
    F2 = np.dot(np.dot(T1, F), T2)
    if verbose:
        for i in range(rows):
            print("F", np.dot(np.dot(pointsImage1[i],F),pointsImage2[i].transpose()), " ", np.dot(np.dot(pointsImage2[0],F),pointsImage1[i].transpose()))
            print("F2", np.dot(np.dot(pointsImage1[i],F2),pointsImage2[i].transpose()), " ", np.dot(np.dot(pointsImage2[0],F2),pointsImage1[i].transpose()))

    return

#getNormalizedF(20, True)