import fundamental
from PIL import Image, ImageDraw
import numpy as np
import essential

def drawOneLine(line, draw,  img):
    xm, ym = img.size
    #x = 0
    y = lambda x: (line[0]*x + line[2])/-line[1]
    #print("Line:", [(0,y(0)), (xm, y(xm))])
    draw.line([(0,y(0)), (xm, y(xm))], fill="red" )

def drawLines():
    F = fundamental.getNormalizedF(20)
    img = Image.open("./data/kronan1.JPG")
    draw = ImageDraw.Draw(img)
    for i in np.random.choice(len(fundamental.pointsImage1), 180):
        point = fundamental.pointsImage1[i]
        line = np.dot(point, F.transpose())
        drawOneLine(line, draw, img)
    img.show()

def drawLinesWithE(samples):
    E = essential.getF(220)
    img = Image.open("./data/kronan1.JPG")
    draw = ImageDraw.Draw(img)
    for i in np.random.choice(len(fundamental.pointsImage1), samples):
        point = fundamental.pointsImage1[i]
        line = np.dot(point, E.transpose())
        drawOneLine(line, draw, img)
    img.show()

drawLinesWithE(1400)
