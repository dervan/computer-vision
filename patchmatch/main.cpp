#include <stdio.h>
#include <opencv2/opencv.hpp>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include <cstring>
#include <math.h>

using namespace cv;
const float stepsize = 0.5;
const float bonus = 1.0;
const float tcol = 10;
const float tgrad = 2;
const float a = 0.9;
const float gm = 10;

int find(const char c, const char * str){
    int i = 0;
    while(str[i]!=0 && str[i] != c)
        i++;
    return i;
}

float computeEnergy(Mat disp, Mat left, Mat right, float penalty, int maxd){
    int xdim = left.cols;
    int ydim = left.rows;
    float energy = 0;
    for(int x=1; x<xdim-maxd; x++){
        for(int y=1; y<ydim-1; y++){
            float tmp = (left.at<float>(y,x)- right.at<float>(y,x+(int)disp.at<float>(y,x)));
            energy += tmp*tmp;
            if((int)disp.at<float>(y,x)!=(int)disp.at<float>(y-1,x))energy+=penalty;
            if((int)disp.at<float>(y,x)!=(int)disp.at<float>(y+1,x))energy+=penalty;
            if((int)disp.at<float>(y,x)!=(int)disp.at<float>(y,x-1))energy+=penalty;
            if((int)disp.at<float>(y,x)!=(int)disp.at<float>(y,x+1))energy+=penalty;
        }
    }
    return energy;
}


#define min2(a,b) (a<b)?(a):(b)
#define min3(a,b,c) ((a<b)?(min(a,c)):(min(b,c)))
#define DIFF_2(a,b,x)  (((a).x-(b).x)*((a).x-(b).x))

#define SUM_DIFF_2(a,b) (DIFF_2(a,b,x)+DIFF_2(a,b,y)+DIFF_2(a,b,z))

float l1d(Point3f a, Point3f b){
    return abs(a.x-b.x)+abs(a.y-b.y)+abs(a.z-b.z);
}

float l1d(Point2f a, Point2f b){
    return abs(a.x-b.x)+abs(a.y-b.y);
}

double matchCost(Mat left, Mat right, int ly, int lx, int ry, int rx, int p, Mat gleft, Mat gright){
    int xdim = left.cols;
    int ydim = left.rows;
    int ym = min3(p, ly, ry);
    int xm = min3(p, lx, rx);
    int yp = min3(p, ydim-ly-1, ydim-ry-1);
    int xp = min3(p, xdim-lx-1, xdim-rx-1);
    float diff;
    Point3f one(1.0, 1.0, 1.0);
    Rect lroi(lx-xm,ly-ym, xm+xp+1, ym+yp+1);
    Rect rroi(rx-xm,ry-ym, xm+xp+1, ym+yp+1);

    /* Get crops */
    Mat lval = left(lroi);
    Mat rval = right(rroi);
    Mat lgrad = gleft(lroi);
    Mat rgrad = gright(rroi);
    Point3f ref = left.at<Point3f>(ly, lx);
    Mat r = Mat::ones(lval.rows, lval.cols, CV_32F);

    MatConstIterator_<Point3f> rit = rval.begin<Point3f>();
    MatConstIterator_<Point2f> grit = rgrad.begin<Point2f>();
    MatConstIterator_<Point3f> lit = lval.begin<Point3f>();
    MatConstIterator_<Point2f> glit = lgrad.begin<Point2f>();

    MatIterator_<float> it = r.begin<float>();
    //std::cout << lval << std::endl;
    //std::cout << rval << std::endl;
    //rval = lval-rval;
    //rval = rval.mul(rval);
    while(it != r.end<float>()){
        //std::cout << *grit << " " << *glit << " => " <<  a*min(l1d(*grit, *glit), tgrad) << " " << lx-rx << "\n";
        diff = (1-a)*min(l1d(*rit,*lit), tcol) + a*min(l1d(*grit, *glit), tgrad);
        (*it) = exp(-l1d(*lit, ref)/gm)*diff;
        it++; rit++; grit++; lit++; glit++;
    }
    //rval = rval.mul(w);
    //std::cout << rval << std::endl << sum(rval)[0] << std::endl;
    //printf("%dx%d, %dx%d %f\n", lval.rows, lval.cols, rval.rows, rval.cols, sum(rval)[0]);
    //std::cout << r << std::endl;
    return sum(r)[0];
}

void propagate(Mat left, Mat right, Mat depth, int p, int maxd, Mat leftGrad, Mat rightGrad){
    static int it = 0;
    it++;
    int xdim = left.cols;
    int ydim = left.rows;
    int d = -1;
    int ex = 0;
    int sx = left.cols - 1;
    int ey = 0;
    int sy = left.rows - 1;
    if(it&1){
        d = 1;
        sx = 0;
        ex = left.cols - 1;
        sy = 0;
        ey = left.rows - 1;
    }
    int scnt = log(maxd)/log(1.0/stepsize)+1;
    float steps[scnt];
    float vals[scnt];
    float penalty = 255*255*(2*p+1)*(2*p+1)*3;
    float val, valx, valy;
    float newstep = 0;

    for(int x = sx; x!=ex;  x+=d){
        for(int y = sy; y!=ey;  y+=d){
            // Correspondence
            if(x+depth.at<float>(y,x) < xdim){
                val = matchCost(left, right, y, x, y, x+depth.at<float>(y,x), p, leftGrad, rightGrad);
            }else{
                val = penalty;
            }
            if(x-d>0 && x-d<xdim && x+depth.at<float>(y,x-d) < xdim){
                valx = matchCost(left, right, y, x, y, x+depth.at<float>(y,x-d), p, leftGrad, rightGrad);
            }else{
                valx = penalty;
            }
            if(y-d>0 && y-d<ydim && x+depth.at<float>(y-d,x) < xdim){
                valy = matchCost(left, right, y, x, y, x+depth.at<float>(y-d,x), p, leftGrad, rightGrad);
            }else{
                valy = penalty;
            }
            //printf("%d %d %d | ", valx, valy, val);

            if(valx<=valy){
                valy = valx;
                newstep =  depth.at<float>(y,x-d);
            }else{
                newstep =  depth.at<float>(y-d,x);

            }

            if(valy<val){
                depth.at<float>(y,x) = newstep;
                val = valy;
            }/*
            if(valx*bonus<val){
                depth.at<float>(y,x) = depth.at<float>(y,x-d);
                val = valx;
            }*/

            for(int k=0;k<scnt;k++){
                newstep = (((float)rand())/(RAND_MAX>>1))-1;
                //printf("%f ", newstep);
                //randu(steps[k], Scalar::all(-1), Scalar::all(1));
                newstep *=maxd*pow(stepsize, k);
                //printf("%f ", newstep);
                newstep += depth.at<float>(y,x);
                //printf("%f | \n", newstep);
                if(false && newstep >0 && newstep<maxd && newstep+x<xdim){
                    valx = matchCost(left, right, y, x, y, x+newstep, p, leftGrad, rightGrad);
                    if(valx < val){
                        depth.at<float>(y,x) = newstep;
                        val = valx;
                    }
                }
            }
            //printf("\n");
        }
        double min,max;
        Mat ndepth;
        cv::minMaxLoc(depth, &min, &max);
        ndepth = depth / max;
        imshow("Display Image", ndepth);
        waitKey(1);
        if((x%100)==0)
            printf("%d => %d => %d \n",sx, x, ex);
    }
    //randu(depth, Scalar::all(0), Scalar::all(maxd));
}

int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        printf("usage: %s dataset\n", argv[0]);
        return -1;
    }
    FILE * fp;
    char * leftFile  = NULL;
    char * rightFile = NULL;
    char * tmp = NULL;
    int patchSize = 0;
    size_t len = 0;
    int disparities = 0;

    fp = fopen(argv[1], "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);
    int nl = 0;
    getline(&leftFile, &len, fp);
    nl = find('\n', leftFile);
    leftFile[nl] = 0;
    getline(&rightFile, &len, fp);
    nl = find('\n', rightFile);
    rightFile[nl] = 0;
    getline(&tmp, &len, fp);
    sscanf(tmp, "%d", &patchSize);
    getline(&tmp, &len, fp);
    sscanf(tmp, "%d", &disparities);
    fclose(fp); 
    Mat_<Point3f> left;
    Mat leftint;
    Mat_<float> leftGray;
    Mat_<Point2f> leftGrad;
    Mat_<Point3f> right;
    Mat rightint;
    Mat_<float> rightGray;
    Mat_<Point2f> rightGrad;
    leftint = imread(leftFile );
    rightint = imread(rightFile);
    leftint.convertTo(left, CV_32F, 1.0/255);
    rightint.convertTo(right, CV_32F, 1.0/255);
    printf("Loading %s %s with params %d %d", leftFile, rightFile, patchSize, disparities);
    if ( !left.data || !right.data )
    {
        printf("No image data \n");
        return -1;
    }

    cvtColor(left, leftGray, COLOR_RGB2GRAY );
    cvtColor(right, rightGray, COLOR_RGB2GRAY );
    int scale = 1;
    int delta = 0;
    Mat lgrad_x, lgrad_y;
    Mat rgrad_x, rgrad_y;
    
    Sobel(leftGray, lgrad_x, CV_32F, 1, 0, 1, scale, delta, BORDER_DEFAULT );
    Sobel(leftGray, lgrad_y, CV_32F, 0, 1, 1, scale, delta, BORDER_DEFAULT );
    leftGrad = Mat::zeros(lgrad_x.rows, lgrad_x.cols, CV_32F);
    Sobel(rightGray, rgrad_x, CV_32F, 1, 0, 1, scale, delta, BORDER_DEFAULT );
    Sobel(rightGray, rgrad_y, CV_32F, 0, 1, 1, scale, delta, BORDER_DEFAULT );
    rightGrad = Mat::zeros(rgrad_x.rows, rgrad_x.cols, CV_32F);
    MatIterator_<Point2f> lit = leftGrad.begin();
    MatConstIterator_<float> litx = lgrad_x.begin<float>();
    MatConstIterator_<float> lity = lgrad_y.begin<float>();
    while(lit != leftGrad.end()){
        *lit = Point2f(*litx, *lity);
        lit++;
        litx++;
        lity++;
    }


    MatIterator_<Point2f> rit = rightGrad.begin();
    MatConstIterator_<float> ritx = rgrad_x.begin<float>();
    MatConstIterator_<float> rity = rgrad_y.begin<float>();
    while(rit != rightGrad.end()){
        *rit = Point2f(*ritx, *rity);
        rit++;
        ritx++;
        rity++;
    }

    Mat depth(left.rows, left.cols, CV_32F);
    Mat ndepth(left.rows, left.cols, CV_32F);
    randu(depth, Scalar::all(0), Scalar::all(disparities));
    namedWindow("Display Image", WINDOW_AUTOSIZE );
    double min, max;

    for(int i=0; i<5; i++){
        cv::minMaxLoc(depth, &min, &max);
        ndepth = depth / max;
        imshow("Display Image", ndepth);
        waitKey(3);
        propagate(left, right, depth, patchSize, disparities, leftGrad, rightGrad);
        cv::minMaxLoc(depth, &min, &max);
    }
    imshow("Display Image", ndepth);
    printf("Energy: %f \n", computeEnergy(depth, left, right, 100, disparities));
    waitKey(0);


    return 0;
}
